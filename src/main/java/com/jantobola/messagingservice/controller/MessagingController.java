package com.jantobola.messagingservice.controller;

import com.jantobola.messagingservice.model.SlackMessageRequest;
import com.jantobola.messagingservice.service.SlackMessageService;
import lombok.RequiredArgsConstructor;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/messaging")
@RequiredArgsConstructor
public class MessagingController {

    private final SlackMessageService slackMessageService;

    @PostMapping("/slack")
    public ResponseEntity<Void> notifySlack(@Valid @RequestBody SlackMessageRequest messageRequest) {
        slackMessageService.sendMessage(messageRequest);
        return ResponseEntity.noContent().build();
    }

}

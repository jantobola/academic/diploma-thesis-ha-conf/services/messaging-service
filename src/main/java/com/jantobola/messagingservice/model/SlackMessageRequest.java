package com.jantobola.messagingservice.model;

import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotBlank;

@Data
@ToString
public class SlackMessageRequest {

    @NotBlank
    private String title;

    @NotBlank
    private String message;

}

package com.jantobola.messagingservice.properties;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
@Component
@ConfigurationProperties("messaging")
public class MessagingProperties {

    @Valid
    private Slack slack = new Slack();

    @Data
    public static class Slack {

        @NotBlank
        private String webhookUrl;
    }

}

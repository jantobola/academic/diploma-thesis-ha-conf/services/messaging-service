package com.jantobola.messagingservice.configuration;

import com.jantobola.messagingservice.messaging.SlackChannel;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;

@EnableBinding(SlackChannel.class)
@Configuration
public class StreamsConfiguration {
}

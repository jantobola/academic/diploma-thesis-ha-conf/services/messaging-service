package com.jantobola.messagingservice.messaging.listener;

import com.jantobola.messagingservice.messaging.SlackChannel;
import com.jantobola.messagingservice.model.SlackMessageRequest;
import com.jantobola.messagingservice.service.SlackMessageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class SlackChannelListener {

    private final SlackMessageService slackMessageService;

    @StreamListener(SlackChannel.INPUT)
    public void handleGreetings(@Payload SlackMessageRequest message) {
        log.info("New message received, sending to Slack.");
        slackMessageService.sendMessage(message);
    }

}

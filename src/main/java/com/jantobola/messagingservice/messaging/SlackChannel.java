package com.jantobola.messagingservice.messaging;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface SlackChannel {

    String INPUT = "slack-channel-in";

    @Input(INPUT)
    SubscribableChannel inboundSlackChannel();

}

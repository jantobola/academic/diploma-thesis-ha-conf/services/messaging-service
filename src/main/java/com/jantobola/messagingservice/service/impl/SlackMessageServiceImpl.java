package com.jantobola.messagingservice.service.impl;

import com.jantobola.messagingservice.model.SlackMessageRequest;
import com.jantobola.messagingservice.properties.MessagingProperties;
import com.jantobola.messagingservice.service.SlackMessageService;
import lombok.RequiredArgsConstructor;
import net.gpedro.integrations.slack.SlackApi;
import net.gpedro.integrations.slack.SlackMessage;

import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SlackMessageServiceImpl implements SlackMessageService {

    private final MessagingProperties messagingProperties;

    @Override
    public void sendMessage(SlackMessageRequest messageRequest) {
        var slackApi = new SlackApi(messagingProperties.getSlack().getWebhookUrl());
        var message = new SlackMessage();
        message.setUsername(messageRequest.getTitle());
        message.setText(messageRequest.getMessage());
        slackApi.call(message);
    }
}

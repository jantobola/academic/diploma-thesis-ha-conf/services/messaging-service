package com.jantobola.messagingservice.service;

import com.jantobola.messagingservice.model.SlackMessageRequest;

public interface SlackMessageService {

    void sendMessage(SlackMessageRequest messageRequest);

}
